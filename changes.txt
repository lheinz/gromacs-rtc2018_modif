gromacs/mdlib/vcm.cpp: line 198 to 211 -> stop angular correction if group consists of single atom
gromacs/mdlib/vcm.cpp: line 966 to 973 -> kill large-VCM warnings
gromacs/mdlib/vcm.cpp: line 413, 418, 423 -> remove outerx, outerv
gromacs/utility/cstringutil.h: line 60 -> increased STRLEN to accommodate longer lines in mdp-file
gromacs/gmxpreprocess/readir.cpp: line 88 -> increase MAXPTR to accommodate more index groups
gromacs/gmxpreprocess/readir.cpp: line 89 -> increase NOGID to accommodate more index groups

gromacs/topology/topology.h: line 90 -> unsigned char* to unsigned int*
programs/mdrun/membed.cpp: line 698 -> unsigned char* to unsigned int*
and many more identical changes
