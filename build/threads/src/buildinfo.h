/*
 * This file is part of the GROMACS molecular simulation package.
 *
 * Copyright (c) 2012,2013,2014,2015,2017, by the GROMACS development team, led by
 * Mark Abraham, David van der Spoel, Berk Hess, and Erik Lindahl,
 * and including many others, as listed in the AUTHORS file in the
 * top-level source directory and at http://www.gromacs.org.
 *
 * GROMACS is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public License
 * as published by the Free Software Foundation; either version 2.1
 * of the License, or (at your option) any later version.
 *
 * GROMACS is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with GROMACS; if not, see
 * http://www.gnu.org/licenses, or write to the Free Software Foundation,
 * Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA.
 *
 * If you want to redistribute modifications to GROMACS, please
 * consider that scientific software is very special. Version
 * control is crucial - bugs must be traceable. We will be happy to
 * consider code for inclusion in the official distribution, but
 * derived work must not be called official GROMACS. Details are found
 * in the README & COPYING files - if they are missing, get the
 * official version at http://www.gromacs.org.
 *
 * To help us fund GROMACS development, we humbly ask that you cite
 * the research papers on the package. Check out http://www.gromacs.org.
 */
/*! \internal \file
 * \brief
 * Build information from the build system.
 *
 * Used for log and version output.
 */

/** Hardware and OS version for build host */
#define BUILD_HOST              "Linux 3.10.0-693.21.1.el7.x86_64 x86_64"

/** Date and time for build */
#define BUILD_TIME              "2019-01-31 17:38:07"

/** User doing build */
#define BUILD_USER              "lheinz@owl [CMAKE]"

/** CPU vendor for build host */
#define BUILD_CPU_VENDOR        "Intel"

/** CPU brand for build host */
#define BUILD_CPU_BRAND         "Intel(R) Xeon(R) CPU           E5430  @ 2.66GHz"

/** CPU family for build host */
#define BUILD_CPU_FAMILY        6

/** CPU model for build host */
#define BUILD_CPU_MODEL         23

/** CPU stepping for build host */
#define BUILD_CPU_STEPPING      6

/** CPU feature list for build host */
#define BUILD_CPU_FEATURES      "apic clfsh cmov cx8 cx16 intel lahf mmx msr pdcm pse sse2 sse3 sse4.1 ssse3"

/** C compiler used to build */
#define BUILD_C_COMPILER        "/sw/cluster/gcc-5.4.0/bin/gcc GNU 5.4.0"

/** C compiler flags used to build */
#define BUILD_CFLAGS            " -march=core-avx2     -O2 -DNDEBUG -funroll-all-loops -fexcess-precision=fast  "

/** C++ compiler flags used to build, or empty string if no C++ */
#define BUILD_CXX_COMPILER      "/sw/cluster/gcc-5.4.0/bin/g++ GNU 5.4.0"

/** C++ compiler flags used to build */
#define BUILD_CXXFLAGS          " -march=core-avx2    -std=c++11   -O2 -DNDEBUG -funroll-all-loops -fexcess-precision=fast  "

/** Installation prefix (default location of data files) */
#define CMAKE_INSTALL_PREFIX    "/home/lheinz/Install/gromacs2018_COM"

/** Source directory for the build */
#define CMAKE_SOURCE_DIR        "/home/lheinz/Install/gromacs2018_COM/gromacs-rtc2018_modif_unsigned_char_to_int"

/** Binary directory for the build */
#define CMAKE_BINARY_DIR        "/home/lheinz/Install/gromacs2018_COM/gromacs-rtc2018_modif_unsigned_char_to_int/build/threads"

/** Location of data files in the installation directory */
#define DATA_INSTALL_DIR        "share/gromacs"

/** CUDA compiler version information */
#define CUDA_COMPILER_INFO "/cm/shared/apps/cuda80/toolkit/8.0.61/bin/nvcc nvcc: NVIDIA (R) Cuda compiler driver;Copyright (c) 2005-2016 NVIDIA Corporation;Built on Tue_Jan_10_13:22:03_CST_2017;Cuda compilation tools, release 8.0, V8.0.61"

/** CUDA compiler flags */
#define CUDA_COMPILER_FLAGS "-gencode;arch=compute_20,code=sm_20;-gencode;arch=compute_30,code=sm_30;-gencode;arch=compute_35,code=sm_35;-gencode;arch=compute_37,code=sm_37;-gencode;arch=compute_50,code=sm_50;-gencode;arch=compute_52,code=sm_52;-gencode;arch=compute_60,code=sm_60;-gencode;arch=compute_61,code=sm_61;-gencode;arch=compute_60,code=compute_60;-gencode;arch=compute_61,code=compute_61;-use_fast_math;-Wno-deprecated-gpu-targets;;; ;-march=core-avx2;-std=c++11;-O2;-DNDEBUG;-funroll-all-loops;-fexcess-precision=fast;"

/** OpenCL include dir */
#define OPENCL_INCLUDE_DIR ""

/** OpenCL library */
#define OPENCL_LIBRARY ""

/** OpenCL version */
#define OPENCL_VERSION_STRING ""
