# Install script for directory: /home/lheinz/Install/gromacs2018_COM/gromacs-rtc2018_modif_unsigned_char_to_int/src/gromacs

# Set the install prefix
if(NOT DEFINED CMAKE_INSTALL_PREFIX)
  set(CMAKE_INSTALL_PREFIX "/home/lheinz/Install/gromacs2018_COM")
endif()
string(REGEX REPLACE "/$" "" CMAKE_INSTALL_PREFIX "${CMAKE_INSTALL_PREFIX}")

# Set the install configuration name.
if(NOT DEFINED CMAKE_INSTALL_CONFIG_NAME)
  if(BUILD_TYPE)
    string(REGEX REPLACE "^[^A-Za-z0-9_]+" ""
           CMAKE_INSTALL_CONFIG_NAME "${BUILD_TYPE}")
  else()
    set(CMAKE_INSTALL_CONFIG_NAME "Release")
  endif()
  message(STATUS "Install configuration: \"${CMAKE_INSTALL_CONFIG_NAME}\"")
endif()

# Set the component getting installed.
if(NOT CMAKE_INSTALL_COMPONENT)
  if(COMPONENT)
    message(STATUS "Install component: \"${COMPONENT}\"")
    set(CMAKE_INSTALL_COMPONENT "${COMPONENT}")
  else()
    set(CMAKE_INSTALL_COMPONENT)
  endif()
endif()

# Install shared libraries without execute permission?
if(NOT DEFINED CMAKE_INSTALL_SO_NO_EXE)
  set(CMAKE_INSTALL_SO_NO_EXE "0")
endif()

if(NOT CMAKE_INSTALL_LOCAL_ONLY)
  # Include the install script for each subdirectory.
  include("/home/lheinz/Install/gromacs2018_COM/gromacs-rtc2018_modif_unsigned_char_to_int/build/threads/src/gromacs/gmxlib/cmake_install.cmake")
  include("/home/lheinz/Install/gromacs2018_COM/gromacs-rtc2018_modif_unsigned_char_to_int/build/threads/src/gromacs/mdlib/cmake_install.cmake")
  include("/home/lheinz/Install/gromacs2018_COM/gromacs-rtc2018_modif_unsigned_char_to_int/build/threads/src/gromacs/applied-forces/cmake_install.cmake")
  include("/home/lheinz/Install/gromacs2018_COM/gromacs-rtc2018_modif_unsigned_char_to_int/build/threads/src/gromacs/listed-forces/cmake_install.cmake")
  include("/home/lheinz/Install/gromacs2018_COM/gromacs-rtc2018_modif_unsigned_char_to_int/build/threads/src/gromacs/commandline/cmake_install.cmake")
  include("/home/lheinz/Install/gromacs2018_COM/gromacs-rtc2018_modif_unsigned_char_to_int/build/threads/src/gromacs/domdec/cmake_install.cmake")
  include("/home/lheinz/Install/gromacs2018_COM/gromacs-rtc2018_modif_unsigned_char_to_int/build/threads/src/gromacs/ewald/cmake_install.cmake")
  include("/home/lheinz/Install/gromacs2018_COM/gromacs-rtc2018_modif_unsigned_char_to_int/build/threads/src/gromacs/fft/cmake_install.cmake")
  include("/home/lheinz/Install/gromacs2018_COM/gromacs-rtc2018_modif_unsigned_char_to_int/build/threads/src/gromacs/gpu_utils/cmake_install.cmake")
  include("/home/lheinz/Install/gromacs2018_COM/gromacs-rtc2018_modif_unsigned_char_to_int/build/threads/src/gromacs/hardware/cmake_install.cmake")
  include("/home/lheinz/Install/gromacs2018_COM/gromacs-rtc2018_modif_unsigned_char_to_int/build/threads/src/gromacs/linearalgebra/cmake_install.cmake")
  include("/home/lheinz/Install/gromacs2018_COM/gromacs-rtc2018_modif_unsigned_char_to_int/build/threads/src/gromacs/math/cmake_install.cmake")
  include("/home/lheinz/Install/gromacs2018_COM/gromacs-rtc2018_modif_unsigned_char_to_int/build/threads/src/gromacs/mdrunutility/cmake_install.cmake")
  include("/home/lheinz/Install/gromacs2018_COM/gromacs-rtc2018_modif_unsigned_char_to_int/build/threads/src/gromacs/mdtypes/cmake_install.cmake")
  include("/home/lheinz/Install/gromacs2018_COM/gromacs-rtc2018_modif_unsigned_char_to_int/build/threads/src/gromacs/onlinehelp/cmake_install.cmake")
  include("/home/lheinz/Install/gromacs2018_COM/gromacs-rtc2018_modif_unsigned_char_to_int/build/threads/src/gromacs/options/cmake_install.cmake")
  include("/home/lheinz/Install/gromacs2018_COM/gromacs-rtc2018_modif_unsigned_char_to_int/build/threads/src/gromacs/pbcutil/cmake_install.cmake")
  include("/home/lheinz/Install/gromacs2018_COM/gromacs-rtc2018_modif_unsigned_char_to_int/build/threads/src/gromacs/random/cmake_install.cmake")
  include("/home/lheinz/Install/gromacs2018_COM/gromacs-rtc2018_modif_unsigned_char_to_int/build/threads/src/gromacs/tables/cmake_install.cmake")
  include("/home/lheinz/Install/gromacs2018_COM/gromacs-rtc2018_modif_unsigned_char_to_int/build/threads/src/gromacs/taskassignment/cmake_install.cmake")
  include("/home/lheinz/Install/gromacs2018_COM/gromacs-rtc2018_modif_unsigned_char_to_int/build/threads/src/gromacs/timing/cmake_install.cmake")
  include("/home/lheinz/Install/gromacs2018_COM/gromacs-rtc2018_modif_unsigned_char_to_int/build/threads/src/gromacs/topology/cmake_install.cmake")
  include("/home/lheinz/Install/gromacs2018_COM/gromacs-rtc2018_modif_unsigned_char_to_int/build/threads/src/gromacs/trajectory/cmake_install.cmake")
  include("/home/lheinz/Install/gromacs2018_COM/gromacs-rtc2018_modif_unsigned_char_to_int/build/threads/src/gromacs/utility/cmake_install.cmake")
  include("/home/lheinz/Install/gromacs2018_COM/gromacs-rtc2018_modif_unsigned_char_to_int/build/threads/src/gromacs/fileio/cmake_install.cmake")
  include("/home/lheinz/Install/gromacs2018_COM/gromacs-rtc2018_modif_unsigned_char_to_int/build/threads/src/gromacs/swap/cmake_install.cmake")
  include("/home/lheinz/Install/gromacs2018_COM/gromacs-rtc2018_modif_unsigned_char_to_int/build/threads/src/gromacs/essentialdynamics/cmake_install.cmake")
  include("/home/lheinz/Install/gromacs2018_COM/gromacs-rtc2018_modif_unsigned_char_to_int/build/threads/src/gromacs/pulling/cmake_install.cmake")
  include("/home/lheinz/Install/gromacs2018_COM/gromacs-rtc2018_modif_unsigned_char_to_int/build/threads/src/gromacs/awh/cmake_install.cmake")
  include("/home/lheinz/Install/gromacs2018_COM/gromacs-rtc2018_modif_unsigned_char_to_int/build/threads/src/gromacs/simd/cmake_install.cmake")
  include("/home/lheinz/Install/gromacs2018_COM/gromacs-rtc2018_modif_unsigned_char_to_int/build/threads/src/gromacs/imd/cmake_install.cmake")
  include("/home/lheinz/Install/gromacs2018_COM/gromacs-rtc2018_modif_unsigned_char_to_int/build/threads/src/gromacs/compat/tests/cmake_install.cmake")

endif()

