# CMake generated Testfile for 
# Source directory: /home/lheinz/Install/gromacs2018_COM/gromacs-rtc2018_modif_unsigned_char_to_int/src/programs/mdrun/tests
# Build directory: /home/lheinz/Install/gromacs2018_COM/gromacs-rtc2018_modif_unsigned_char_to_int/build/serial/src/programs/mdrun/tests
# 
# This file includes the relevant testing commands required for 
# testing this directory and lists subdirectories to be tested as well.
add_test(MdrunTests "/home/lheinz/Install/gromacs2018_COM/gromacs-rtc2018_modif_unsigned_char_to_int/build/serial/bin/mdrun-test" "--gtest_output=xml:/home/lheinz/Install/gromacs2018_COM/gromacs-rtc2018_modif_unsigned_char_to_int/build/serial/Testing/Temporary/MdrunTests.xml")
set_tests_properties(MdrunTests PROPERTIES  LABELS "GTest;IntegrationTest" TIMEOUT "120")
