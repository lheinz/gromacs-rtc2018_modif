# CMake generated Testfile for 
# Source directory: /home/lheinz/Install/gromacs2018_COM/gromacs-rtc2018_modif_unsigned_char_to_int/src/gromacs/correlationfunctions/tests
# Build directory: /home/lheinz/Install/gromacs2018_COM/gromacs-rtc2018_modif_unsigned_char_to_int/build/serial/src/gromacs/correlationfunctions/tests
# 
# This file includes the relevant testing commands required for 
# testing this directory and lists subdirectories to be tested as well.
add_test(CorrelationsTest "/home/lheinz/Install/gromacs2018_COM/gromacs-rtc2018_modif_unsigned_char_to_int/build/serial/bin/correlations-test" "--gtest_output=xml:/home/lheinz/Install/gromacs2018_COM/gromacs-rtc2018_modif_unsigned_char_to_int/build/serial/Testing/Temporary/CorrelationsTest.xml")
set_tests_properties(CorrelationsTest PROPERTIES  LABELS "GTest;UnitTest" TIMEOUT "30")
