# Install script for directory: /home/lheinz/Install/gromacs2018_COM/gromacs-rtc2018_modif_unsigned_char_to_int/src/gromacs/math

# Set the install prefix
if(NOT DEFINED CMAKE_INSTALL_PREFIX)
  set(CMAKE_INSTALL_PREFIX "/home/lheinz/Install/gromacs2018_COM")
endif()
string(REGEX REPLACE "/$" "" CMAKE_INSTALL_PREFIX "${CMAKE_INSTALL_PREFIX}")

# Set the install configuration name.
if(NOT DEFINED CMAKE_INSTALL_CONFIG_NAME)
  if(BUILD_TYPE)
    string(REGEX REPLACE "^[^A-Za-z0-9_]+" ""
           CMAKE_INSTALL_CONFIG_NAME "${BUILD_TYPE}")
  else()
    set(CMAKE_INSTALL_CONFIG_NAME "Release")
  endif()
  message(STATUS "Install configuration: \"${CMAKE_INSTALL_CONFIG_NAME}\"")
endif()

# Set the component getting installed.
if(NOT CMAKE_INSTALL_COMPONENT)
  if(COMPONENT)
    message(STATUS "Install component: \"${COMPONENT}\"")
    set(CMAKE_INSTALL_COMPONENT "${COMPONENT}")
  else()
    set(CMAKE_INSTALL_COMPONENT)
  endif()
endif()

# Install shared libraries without execute permission?
if(NOT DEFINED CMAKE_INSTALL_SO_NO_EXE)
  set(CMAKE_INSTALL_SO_NO_EXE "0")
endif()

if("${CMAKE_INSTALL_COMPONENT}" STREQUAL "development" OR NOT CMAKE_INSTALL_COMPONENT)
  file(INSTALL DESTINATION "${CMAKE_INSTALL_PREFIX}/include/gromacs/math" TYPE FILE FILES
    "/home/lheinz/Install/gromacs2018_COM/gromacs-rtc2018_modif_unsigned_char_to_int/src/gromacs/math/3dtransforms.h"
    "/home/lheinz/Install/gromacs2018_COM/gromacs-rtc2018_modif_unsigned_char_to_int/src/gromacs/math/do_fit.h"
    "/home/lheinz/Install/gromacs2018_COM/gromacs-rtc2018_modif_unsigned_char_to_int/src/gromacs/math/functions.h"
    "/home/lheinz/Install/gromacs2018_COM/gromacs-rtc2018_modif_unsigned_char_to_int/src/gromacs/math/gmxcomplex.h"
    "/home/lheinz/Install/gromacs2018_COM/gromacs-rtc2018_modif_unsigned_char_to_int/src/gromacs/math/units.h"
    "/home/lheinz/Install/gromacs2018_COM/gromacs-rtc2018_modif_unsigned_char_to_int/src/gromacs/math/utilities.h"
    "/home/lheinz/Install/gromacs2018_COM/gromacs-rtc2018_modif_unsigned_char_to_int/src/gromacs/math/vec.h"
    "/home/lheinz/Install/gromacs2018_COM/gromacs-rtc2018_modif_unsigned_char_to_int/src/gromacs/math/vectypes.h"
    "/home/lheinz/Install/gromacs2018_COM/gromacs-rtc2018_modif_unsigned_char_to_int/src/gromacs/math/paddedvector.h"
    )
endif()

if(NOT CMAKE_INSTALL_LOCAL_ONLY)
  # Include the install script for each subdirectory.
  include("/home/lheinz/Install/gromacs2018_COM/gromacs-rtc2018_modif_unsigned_char_to_int/build/serial/src/gromacs/math/tests/cmake_install.cmake")

endif()

