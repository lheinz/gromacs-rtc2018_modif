# CMake generated Testfile for 
# Source directory: /home/lheinz/Install/gromacs2018_COM/gromacs-rtc2018_modif_unsigned_char_to_int/src/gromacs/gmxpreprocess/tests
# Build directory: /home/lheinz/Install/gromacs2018_COM/gromacs-rtc2018_modif_unsigned_char_to_int/build/serial/src/gromacs/gmxpreprocess/tests
# 
# This file includes the relevant testing commands required for 
# testing this directory and lists subdirectories to be tested as well.
add_test(GmxPreprocessTests "/home/lheinz/Install/gromacs2018_COM/gromacs-rtc2018_modif_unsigned_char_to_int/build/serial/bin/gmxpreprocess-test" "--gtest_output=xml:/home/lheinz/Install/gromacs2018_COM/gromacs-rtc2018_modif_unsigned_char_to_int/build/serial/Testing/Temporary/GmxPreprocessTests.xml")
set_tests_properties(GmxPreprocessTests PROPERTIES  LABELS "GTest;UnitTest" TIMEOUT "30")
